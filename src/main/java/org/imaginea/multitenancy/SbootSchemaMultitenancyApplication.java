package org.imaginea.multitenancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbootSchemaMultitenancyApplication {

  public static void main(String[] args) {
    SpringApplication.run(SbootSchemaMultitenancyApplication.class, args);
  }


}
