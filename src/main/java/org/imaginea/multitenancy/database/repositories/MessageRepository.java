package org.imaginea.multitenancy.database.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import org.imaginea.multitenancy.database.entities.Message;

public interface MessageRepository extends PagingAndSortingRepository<Message, String> {

}
