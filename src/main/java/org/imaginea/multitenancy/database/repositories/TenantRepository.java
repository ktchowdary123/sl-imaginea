package org.imaginea.multitenancy.database.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import org.imaginea.multitenancy.database.entities.Tenant;

public interface TenantRepository extends PagingAndSortingRepository<Tenant, String> {

}
